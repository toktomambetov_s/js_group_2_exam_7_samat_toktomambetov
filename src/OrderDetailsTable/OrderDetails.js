import React from 'react';
import './OrderDetails.css';

const OrderDetails = props => {
    return (
        <div className="OrderDetails">
            <h1>Order Details:</h1>
            <p>Total: {props.currentPrice}</p>
        </div>
    );
};

export default OrderDetails;