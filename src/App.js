import React, { Component } from 'react';
import './App.css';
import OrderDetails from "./OrderDetailsTable/OrderDetails";
import ItemsTable from "./AddItemsTable/ItemsTable";
import Hamburger from "./components/Hamburger/Hamburger";
import Cheeseburger from "./components/Cheeseburger/Cheeseburger";
import Fries from "./components/Fries/Fries";
import Coffee from "./components/Coffee/Coffee";
import Tea from "./components/Tea/Tea";
import Cola from "./components/Cola/Cola";

class App extends Component {
    state = {
        menu: [
            {name: 'Hamburger', price: 80},
            {name: 'Cheeseburger', price: 90},
            {name: 'Fries', price: 45},
            {name: 'Coffee', price: 70},
            {name: 'Tea', price: 50},
            {name: 'Cola', price: 40},
        ],
        orderList: [
            {name: 'Hamburger', price: 80, amount: 0, id: 0},
            {name: 'Cheeseburger', price: 90, amount: 0, id: 1},
            {name: 'Fries', price: 45, amount: 0, id: 2},
            {name: 'Coffee', price: 70, amount: 0, id: 3},
            {name: 'Tea', price: 50, amount: 0, id: 4},
            {name: 'Cola', price: 40, amount: 0, id: 5},
        ],
        currentPrice: 0,
    };

    AddItem = (id) => {
        const index = this.state.orderList.findIndex(m => m.id === id);
        const item = {...this.state.orderList[index]};
        item.amount++;
        this.state.currentPrice += item.price;

        const items = [...this.state.orderList];
        items[index] = item;

        this.setState({items});
    };

  render() {
    return (
      <div className="App">
        <OrderDetails currentPrice={this.state.currentPrice}/>

          <div className="Menu">
          <ItemsTable/>
              <Hamburger name={this.state.menu[0].name} price={this.state.menu[0].price} addItem={this.AddItem}/>
              <Cheeseburger name={this.state.menu[1].name} price={this.state.menu[1].price}/>
              <Fries name={this.state.menu[2].name} price={this.state.menu[2].price}/>
              <Coffee name={this.state.menu[3].name} price={this.state.menu[3].price}/>
              <Tea name={this.state.menu[4].name} price={this.state.menu[4].price}/>
              <Cola name={this.state.menu[5].name} price={this.state.menu[5].price}/>
          </div>

      </div>
    );
  }
}

export default App;
