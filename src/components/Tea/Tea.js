import React from 'react';

const Tea = props => {
    return (
        <div>
            <button className={Tea}>
                <p>{props.name}</p>
                <p>Price: {props.price} KGS</p>
            </button>
        </div>
    )
};

export default Tea;