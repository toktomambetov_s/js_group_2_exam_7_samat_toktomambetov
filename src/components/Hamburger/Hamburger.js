import React from 'react';

const Hamburger = props => {
    return (
        <div>
            <button className={Hamburger} onClick={props.AddItem}>
                <p>{props.name}</p>
                <p>Price: {props.price} KGS</p>
            </button>
        </div>
    )
};

export default Hamburger;