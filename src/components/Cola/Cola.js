import React from 'react';

const Cola = props => {
    return (
        <div>
            <button className={Cola}>
                <p>{props.name}</p>
                <p>Price: {props.price} KGS</p>
            </button>
        </div>
    )
};

export default Cola;