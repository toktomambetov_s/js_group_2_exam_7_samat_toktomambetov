import React from 'react';

const Fries = props => {
    return (
        <div>
            <button className={Fries}>
                <p>{props.name}</p>
                <p>Price: {props.price} KGS</p>
            </button>
        </div>
    )
};

export default Fries;