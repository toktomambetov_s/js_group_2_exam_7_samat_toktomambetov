import React from 'react';

const Coffee = props => {
    return (
        <div className={Coffee}>
            <button>
                <p>{props.name}</p>
                <p>Price: {props.price} KGS</p>
            </button>
        </div>
    )
};

export default Coffee;