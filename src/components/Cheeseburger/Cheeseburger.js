import React from 'react';

const Cheeseburger = props => {
    return (
        <div>
            <button className={Cheeseburger}>
                <p>{props.name}</p>
                <p>Price: {props.price} KGS</p>
            </button>
        </div>
    )
};

export default Cheeseburger;